use super::record::Record;
use super::rule::*;
use super::rulebook::Rulebook;

pub trait WhenFn<Args>: Clone {
    fn when(&self, rb: &Rulebook, rec: &Record, args: &Args) -> bool;
}

pub trait ApplyFn<Args, Ret>: Clone {
    fn apply(
        &self,
        rb: &Rulebook,
        rec: &Record,
        args: Args,
        previous_rule: MaybePreviousClosure<Args, Ret>,
    ) -> Ret;
}

pub trait ApplyFnMut<Args, Ret>: Clone {
    fn apply_mut(
        &self,
        rb: &mut Rulebook,
        rec: &mut Record,
        args: Args,
        previous_rule: MaybePreviousClosureMut<Args, Ret>,
    ) -> Ret;
}

impl<T: Fn(&Rulebook, &Record, &Args) -> bool + Clone, Args> WhenFn<Args> for T {
    fn when(&self, rb: &Rulebook, rec: &Record, args: &Args) -> bool {
        self(rb, rec, args)
    }
}

impl<T, Args, Ret> ApplyFn<Args, Ret> for T
where
    T: Fn(&Rulebook, &Record, Args, MaybePreviousClosure<Args, Ret>) -> Ret + Clone,
{
    fn apply(
        &self,
        rb: &Rulebook,
        rec: &Record,
        args: Args,
        previous_rule: MaybePreviousClosure<Args, Ret>,
    ) -> Ret {
        self(rb, rec, args, previous_rule)
    }
}

impl<T, Args, Ret> ApplyFnMut<Args, Ret> for T
where
    T: Clone + Fn(&mut Rulebook, &mut Record, Args, MaybePreviousClosureMut<Args, Ret>) -> Ret,
{
    fn apply_mut(
        &self,
        rb: &mut Rulebook,
        rec: &mut Record,
        args: Args,
        previous_rule: MaybePreviousClosureMut<Args, Ret>,
    ) -> Ret {
        self(rb, rec, args, previous_rule)
    }
}

// Workarounds to help the compiler deduce parameter types
pub fn whenfn<Args>(f: fn(&Rulebook, &Record, &Args) -> bool) -> impl WhenFn<Args> {
    f
}

pub fn applyfn<Args, Ret>(
    f: fn(&Rulebook, &Record, Args, MaybePreviousClosure<Args, Ret>) -> Ret,
) -> impl ApplyFn<Args, Ret> {
    f
}

pub fn applyfn_mut<Args, Ret>(
    f: fn(&mut Rulebook, &mut Record, Args, MaybePreviousClosureMut<Args, Ret>) -> Ret,
) -> impl ApplyFnMut<Args, Ret> {
    f
}

#[derive(Clone)]
pub struct WhenClosure<T: Clone, Args> {
    capture: T,
    function: fn(&Rulebook, &Record, &T, &Args) -> bool,
}

impl<T: Clone, Args: Clone> WhenFn<Args> for WhenClosure<T, Args> {
    fn when(&self, rb: &Rulebook, rec: &Record, args: &Args) -> bool {
        (self.function)(rb, rec, &self.capture, args)
    }
}

pub fn whenclosure<T, Args>(
    capture: T,
    function: fn(&Rulebook, &Record, &T, &Args) -> bool,
) -> impl WhenFn<Args>
where
    T: Clone,
    Args: Clone,
{
    WhenClosure { capture, function }
}

#[derive(Clone)]
pub struct ApplyClosure<T: Clone, Args, Ret> {
    pub capture: T,
    pub function: fn(&Rulebook, &Record, &T, Args, MaybePreviousClosure<Args, Ret>) -> Ret,
}

impl<T: Clone, Args: Clone, Ret: Clone> ApplyFn<Args, Ret> for ApplyClosure<T, Args, Ret> {
    fn apply(
        &self,
        rb: &Rulebook,
        rec: &Record,
        args: Args,
        previous_rule: MaybePreviousClosure<Args, Ret>,
    ) -> Ret {
        (self.function)(rb, rec, &self.capture, args, previous_rule)
    }
}

pub fn applyclosure<T, Args, Ret>(
    capture: T,
    function: fn(&Rulebook, &Record, &T, Args, MaybePreviousClosure<Args, Ret>) -> Ret,
) -> impl ApplyFn<Args, Ret>
where
    T: Clone,
    Args: Clone,
    Ret: Clone,
{
    ApplyClosure { capture, function }
}

#[derive(Clone)]
pub struct ApplyClosureMut<T: Clone, Args, Ret> {
    pub capture: T,
    pub function:
        fn(&mut Rulebook, &mut Record, &T, Args, MaybePreviousClosureMut<Args, Ret>) -> Ret,
}

impl<T: Clone, Args: Clone, Ret: Clone> ApplyFnMut<Args, Ret> for ApplyClosureMut<T, Args, Ret> {
    fn apply_mut(
        &self,
        rb: &mut Rulebook,
        rec: &mut Record,
        args: Args,
        previous_rule: MaybePreviousClosureMut<Args, Ret>,
    ) -> Ret {
        (self.function)(rb, rec, &self.capture, args, previous_rule)
    }
}

pub fn applyclosure_mut<T, Args, Ret>(
    capture: T,
    function: fn(&mut Rulebook, &mut Record, &T, Args, MaybePreviousClosureMut<Args, Ret>) -> Ret,
) -> impl ApplyFnMut<Args, Ret>
where
    T: Clone,
    Args: Clone,
    Ret: Clone,
{
    ApplyClosureMut { capture, function }
}

#[derive(Clone)]
pub struct GenericRule<Args, Ret, W, A>
where
    W: WhenFn<Args>,
    A: ApplyFn<Args, Ret>,
{
    pub when: W,
    pub apply: A,
    _marker: std::marker::PhantomData<(Args, Ret)>,
}

impl<Args, Ret, W, A> GenericRule<Args, Ret, W, A>
where
    W: WhenFn<Args>,
    A: ApplyFn<Args, Ret>,
{
    pub fn new(when: W, apply: A) -> Self {
        Self {
            when,
            apply,
            _marker: std::marker::PhantomData,
        }
    }
}

impl<Args, Ret, W, A> RulePredicate<Args> for GenericRule<Args, Ret, W, A>
where
    Args: 'static + Clone,
    Ret: 'static + Clone,
    W: WhenFn<Args>,
    A: ApplyFn<Args, Ret>,
{
    fn when(&self, rb: &Rulebook, rec: &Record, args: &Args) -> bool {
        self.when.when(rb, rec, args)
    }
}

impl<Args, Ret, W, A> Rule<Args, Ret> for GenericRule<Args, Ret, W, A>
where
    Args: 'static + Clone,
    Ret: 'static + Clone,
    W: 'static + WhenFn<Args>,
    A: 'static + ApplyFn<Args, Ret>,
{
    fn apply(
        &self,
        rb: &Rulebook,
        rec: &Record,
        args: Args,
        previous_rule: MaybePreviousClosure<Args, Ret>,
    ) -> Ret {
        self.apply.apply(rb, rec, args, previous_rule)
    }
    fn box_clone(&self) -> Box<dyn Rule<Args, Ret>> {
        Box::new(self.clone())
    }
}

#[derive(Clone)]
pub struct GenericRuleMut<Args, Ret, W, A>
where
    W: WhenFn<Args>,
    A: ApplyFnMut<Args, Ret>,
{
    pub when: W,
    pub apply: A,
    _marker: std::marker::PhantomData<(Args, Ret)>,
}

impl<Args, Ret, W, A> GenericRuleMut<Args, Ret, W, A>
where
    W: WhenFn<Args>,
    A: ApplyFnMut<Args, Ret>,
{
    pub fn new(when: W, apply: A) -> Self {
        Self {
            when,
            apply,
            _marker: std::marker::PhantomData,
        }
    }
}

impl<Args, Ret, W, A> RulePredicate<Args> for GenericRuleMut<Args, Ret, W, A>
where
    Args: 'static + Clone,
    Ret: 'static + Clone,
    W: WhenFn<Args>,
    A: ApplyFnMut<Args, Ret>,
{
    fn when(&self, rb: &Rulebook, rec: &Record, args: &Args) -> bool {
        self.when.when(rb, rec, args)
    }
}

impl<Args, Ret, W, A> RuleMut<Args, Ret> for GenericRuleMut<Args, Ret, W, A>
where
    Args: 'static + Clone,
    Ret: 'static + Clone,
    W: 'static + WhenFn<Args>,
    A: 'static + ApplyFnMut<Args, Ret>,
{
    fn apply(
        &self,
        rb: &mut Rulebook,
        rec: &mut Record,
        args: Args,
        previous_rule: MaybePreviousClosureMut<Args, Ret>,
    ) -> Ret {
        self.apply.apply_mut(rb, rec, args, previous_rule)
    }
    fn box_clone(&self) -> Box<dyn RuleMut<Args, Ret>> {
        Box::new(self.clone())
    }
}
