use super::record::Record;
use super::rulebook::Rulebook;

pub trait RulePredicate<Args> {
    fn when(&self, rb: &Rulebook, rec: &Record, args: &Args) -> bool;
}

pub trait Rule<Args, Ret>: RulePredicate<Args> {
    fn apply(
        &self,
        rb: &Rulebook,
        rec: &Record,
        args: Args,
        previous_rule: Option<Box<RuleClosure<Args, Ret>>>,
    ) -> Ret;
    fn box_clone(&self) -> Box<dyn Rule<Args, Ret>>;
}

pub trait RuleMut<Args, Ret>: RulePredicate<Args> {
    fn apply(
        &self,
        rb: &mut Rulebook,
        rec: &mut Record,
        args: Args,
        previous_rule: Option<Box<RuleClosureMut<Args, Ret>>>,
    ) -> Ret;
    fn box_clone(&self) -> Box<dyn RuleMut<Args, Ret>>;
}

impl<Args, Ret> Clone for Box<dyn Rule<Args, Ret>> {
    fn clone(&self) -> Self {
        self.box_clone()
    }
    fn clone_from(&mut self, _source: &Self) {}
}

impl<Args, Ret> Clone for Box<dyn RuleMut<Args, Ret>> {
    fn clone(&self) -> Self {
        self.box_clone()
    }
    fn clone_from(&mut self, _source: &Self) {}
}

pub type MaybePreviousClosure<Args, Ret> = Option<Box<RuleClosure<Args, Ret>>>;

pub struct RuleClosure<Args, Ret> {
    pub previous: MaybePreviousClosure<Args, Ret>,
    pub rule: Box<dyn Rule<Args, Ret>>,
}

impl<Args, Ret> RuleClosure<Args, Ret> {
    pub fn apply(self, rb: &Rulebook, rec: &Record, args: Args) -> Ret {
        self.rule.apply(rb, rec, args, self.previous)
    }
}

pub type MaybePreviousClosureMut<Args, Ret> = Option<Box<RuleClosureMut<Args, Ret>>>;

pub struct RuleClosureMut<Args, Ret> {
    pub previous: MaybePreviousClosureMut<Args, Ret>,
    pub rule: Box<dyn RuleMut<Args, Ret>>,
}

impl<Args, Ret> RuleClosureMut<Args, Ret> {
    pub fn apply(self, rb: &mut Rulebook, rec: &mut Record, args: Args) -> Ret {
        self.rule.apply(rb, rec, args, self.previous)
    }
}
