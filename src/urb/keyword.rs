use super::record::Record;
use super::rule::*;
use super::rulebook::Rulebook;
//use std::ops::Deref;

#[derive(Default, Clone)]
pub struct Keyword<Args, Ret> {
    rules: Vec<Box<dyn Rule<Args, Ret>>>,
}

#[derive(Default, Clone)]
pub struct KeywordMut<Args, Ret> {
    rules: Vec<Box<dyn RuleMut<Args, Ret>>>,
}

impl<Args, Ret> Keyword<Args, Ret> {
    pub fn add_rule<T: 'static + Rule<Args, Ret>>(&mut self, rule: T) {
        self.rules.push(Box::new(rule));
    }

    pub fn invoke(&self, rb: &Rulebook, rec: &Record, args: Args) -> Ret {
        let applicable: Vec<&Box<dyn Rule<Args, Ret>>> = self
            .rules
            .iter()
            .filter(|rule| rule.when(rb, rec, &args))
            .collect();
        let mut maybe_closure = None;
        for rule in applicable.iter() {
            maybe_closure = Some(Box::new(RuleClosure {
                previous: maybe_closure,
                rule: rule.box_clone(),
            }));
        }
        if let Some(rule_closure) = maybe_closure {
            rule_closure.apply(rb, rec, args)
        } else {
            panic!("No rule applies");
        }
    }
}

impl<Args, Ret> KeywordMut<Args, Ret> {
    pub fn add_rule<T: 'static + RuleMut<Args, Ret>>(&mut self, rule: T) {
        self.rules.push(Box::new(rule));
    }

    pub fn invoke(&self, rb: &mut Rulebook, rec: &mut Record, args: Args) -> Ret {
        let applicable: Vec<&Box<dyn RuleMut<Args, Ret>>> = self
            .rules
            .iter()
            .filter(|rule| rule.when(rb, rec, &args))
            .collect();
        let mut maybe_closure = None;
        for rule in applicable.iter() {
            maybe_closure = Some(Box::new(RuleClosureMut {
                previous: maybe_closure,
                rule: rule.box_clone(),
            }));
        }
        if let Some(rule_closure) = maybe_closure {
            rule_closure.apply(rb, rec, args)
        } else {
            panic!("No rule applies");
        }
    }
}
