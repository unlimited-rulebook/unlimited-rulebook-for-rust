use super::keyword::{Keyword, KeywordMut};
use super::record::ID;

#[derive(Default, Clone)]
pub struct Rulebook {
    pub new_creature: KeywordMut<(String, i32, i32), ID>,
    pub get_power: Keyword<ID, i32>,
    pub get_toughness: Keyword<ID, i32>,
    pub describe: Keyword<ID, String>,
    pub is_dead: Keyword<ID, bool>,
    pub get_damage: Keyword<ID, i32>,
    pub has_lethal_damage: Keyword<ID, bool>,
    pub take_damage: KeywordMut<(ID, i32), ()>,
    pub cause_damage: KeywordMut<(ID, ID, i32), ()>,
    pub fight: KeywordMut<(ID, ID), ()>,
}
