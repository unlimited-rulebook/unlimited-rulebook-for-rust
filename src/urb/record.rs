use anymap::AnyMap;
use std::collections::HashMap;

pub type ID = usize;

pub struct Record {
    next_id: ID,
    property_tables: AnyMap,
}

impl Clone for Record {
    fn clone(&self) -> Self {
        Record::default()
    }
    fn clone_from(&mut self, _source: &Self) {}
}

impl Default for Record {
    fn default() -> Self {
        Record {
            next_id: 0,
            property_tables: AnyMap::new(),
        }
    }
}

type Table<T> = HashMap<ID, T>;

impl Record {
    pub fn register<T: 'static>(&mut self) {
        self.property_tables.insert(Table::<T>::default());
    }

    pub fn new_id(&mut self) -> ID {
        let id = self.next_id;
        self.next_id += 1;
        id
    }

    pub fn set<T: 'static>(&mut self, id: ID, property: T) {
        if let Some(table) = self.property_tables.get_mut::<Table<T>>() {
            match table.get_mut(&id) {
                Some(old_property) => *old_property = property,
                None => {
                    table.insert(id, property);
                }
            }
        }
    }

    pub fn is<T: 'static>(&self, id: ID) -> bool {
        if let Some(table) = self.property_tables.get::<Table<T>>() {
            table.contains_key(&id)
        } else {
            false
        }
    }

    pub fn get<T: 'static>(&self, id: ID) -> Option<&T> {
        self.property_tables.get::<Table<T>>()?.get(&id)
    }

    pub fn get_mut<T: 'static>(&mut self, id: ID) -> Option<&mut T> {
        self.property_tables.get_mut::<Table<T>>()?.get_mut(&id)
    }

    pub fn all<T: 'static>(&self) -> impl Iterator<Item = (&ID, &T)> {
        if let Some(table) = self.property_tables.get::<Table<T>>() {
            table.iter()
        } else {
            panic![]
        }
    }
}
