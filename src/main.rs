use urb_example::urb::record::Record;
use urb_example::urb::rulebook::Rulebook;

use urb_example::rules::creature::*;
use urb_example::rules::damage::*;
use urb_example::rules::death::*;
use urb_example::rules::fight::*;

fn main() {
    let mut rec = Record::default();
    let mut rb = Rulebook::default();
    define_creature_rules(&mut rec, &mut rb);
    define_death_rules(&mut rec, &mut rb);
    define_damage_rules(&mut rec, &mut rb);
    define_fight_rules(&mut rec, &mut rb);
    let new_creature = rb.new_creature.clone();
    let bear = new_creature.invoke(&mut rb, &mut rec, ("Bear".to_string(), 2, 2));
    let goblin = new_creature.invoke(&mut rb, &mut rec, ("Goblin".to_string(), 1, 1));
    println!("{}", rb.describe.invoke(&rb, &rec, bear));
    println!("{}", rb.describe.invoke(&rb, &rec, goblin));
    let fight = rb.fight.clone();
    fight.invoke(&mut rb, &mut rec, (bear, goblin));
    println!("{}", rb.describe.invoke(&rb, &rec, bear));
    println!("{}", rb.describe.invoke(&rb, &rec, goblin));
}
