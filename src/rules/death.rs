use crate::urb::genericrule::*;
use crate::urb::record::{Record, ID};
use crate::urb::rule::*;
use crate::urb::rulebook::Rulebook;

use crate::rules::creature::*;

pub fn define_death_rules(_: &mut Record, rb: &mut Rulebook) {
    rb.is_dead.add_rule(GenericRule::new(
        whenfn(|_, rec, id| rec.is::<Creature>(*id)),
        applyfn(|rb, rec, id, _| rb.get_toughness.invoke(rb, rec, id) <= 0),
    ));

    rb.describe.add_rule(GenericRule::new(
        whenfn(|rb, rec, id| {
            rec.is::<Named>(*id) && rec.is::<Creature>(*id) && rb.is_dead.invoke(rb, rec, *id)
        }),
        applyfn(|rb, rec, id, prev: MaybePreviousClosure<ID, String>| {
            let mut descr = prev.unwrap().apply(rb, rec, id);
            descr.push_str(" [dead]");
            descr
        }),
    ));
}
