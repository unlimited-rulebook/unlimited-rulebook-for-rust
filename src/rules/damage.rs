use crate::urb::genericrule::*;
use crate::urb::record::{Record, ID};
use crate::urb::rule::*;
use crate::urb::rulebook::Rulebook;

use crate::rules::creature::*;

#[derive(Debug, Clone)]
pub struct Damaged {
    amount: i32,
}

pub fn define_damage_rules(rec: &mut Record, rb: &mut Rulebook) {
    rec.register::<Damaged>();

    rb.get_damage.add_rule(GenericRule::new(
        whenfn(|_, rec, id| rec.is::<Creature>(*id)),
        applyfn(|_, rec, id, _| {
            if let Some(damaged) = rec.get::<Damaged>(id) {
                damaged.amount
            } else {
                0
            }
        }),
    ));

    rb.has_lethal_damage.add_rule(GenericRule::new(
        whenfn(|_, rec, id| rec.is::<Creature>(*id)),
        applyfn(|rb, rec, id, _| {
            rb.get_damage.invoke(rb, rec, id) >= rb.get_toughness.invoke(rb, rec, id)
        }),
    ));

    rb.is_dead.add_rule(GenericRule::new(
        whenfn(|rb, rec, id| rec.is::<Creature>(*id) && rb.has_lethal_damage.invoke(rb, rec, *id)),
        applyfn(|_, _, _, _| true),
    ));

    rb.take_damage.add_rule(GenericRuleMut::new(
        whenfn(|_, rec, (id, _)| rec.is::<Creature>(*id)),
        applyfn_mut(|rb, rec, (id, amount): (ID, i32), _| {
            let damage = rb.get_damage.invoke(rb, rec, id.clone());
            rec.set::<Damaged>(
                id,
                Damaged {
                    amount: damage + amount,
                },
            )
        }),
    ));

    rb.cause_damage.add_rule(GenericRuleMut::new(
        whenfn(|_, rec, (_, target, _)| rec.is::<Creature>(*target)),
        applyfn_mut(|rb, rec, (_, target, amount): (ID, ID, i32), _| {
            let take_damage = rb.take_damage.clone();
            take_damage.invoke(rb, rec, (target, amount));
        }),
    ));

    rb.describe.add_rule(GenericRule::new(
        whenfn(|_, rec, id| {
            rec.is::<Named>(*id) && rec.is::<Creature>(*id) && rec.is::<Damaged>(*id)
        }),
        applyfn(|rb, rec, id, prev: MaybePreviousClosure<ID, String>| {
            let mut descr = prev.unwrap().apply(rb, rec, id);
            let damage = rb.get_damage.invoke(rb, rec, id);
            descr.push_str(format![" [damage: {}]", damage].as_str());
            descr
        }),
    ));
}
