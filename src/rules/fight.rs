use crate::urb::genericrule::*;
use crate::urb::record::{Record, ID};
use crate::urb::rulebook::Rulebook;

use crate::rules::creature::*;

pub fn define_fight_rules(_: &mut Record, rb: &mut Rulebook) {
    rb.fight.add_rule(GenericRuleMut::new(
        whenfn(|_, rec, (attacker, defender)| {
            rec.is::<Creature>(*attacker) && rec.is::<Creature>(*defender)
        }),
        applyfn_mut(|rb, rec, (attacker, defender): (ID, ID), _| {
            let attacker_power = rb.get_power.invoke(rb, rec, attacker);
            let defender_power = rb.get_power.invoke(rb, rec, defender);
            let cause_damage = rb.cause_damage.clone();
            cause_damage.invoke(rb, rec, (attacker, defender, attacker_power));
            cause_damage.invoke(rb, rec, (defender, attacker, defender_power));
        }),
    ));
}
