use crate::urb::genericrule::*;
use crate::urb::record::Record;
use crate::urb::rulebook::Rulebook;

#[derive(Debug, Clone)]
pub struct Named {
    name: String,
}

#[derive(Debug, Clone)]
pub struct Creature {
    power: i32,
    toughness: i32,
}

pub fn define_creature_rules(rec: &mut Record, rb: &mut Rulebook) {
    rec.register::<Named>();
    rec.register::<Creature>();

    rb.new_creature.add_rule(GenericRuleMut::new(
        whenfn(|_, _, _| true),
        applyfn_mut(|_, rec, (name, power, toughness), _| {
            let id = rec.new_id();
            rec.set::<Named>(id, Named { name });
            rec.set::<Creature>(id, Creature { power, toughness });
            id
        }),
    ));

    rb.get_power.add_rule(GenericRule::new(
        whenfn(|_, rec, id| rec.is::<Creature>(*id)),
        applyfn(|_, rec, id, _| rec.get::<Creature>(id).unwrap().power),
    ));

    rb.get_toughness.add_rule(GenericRule::new(
        whenfn(|_, rec, id| rec.is::<Creature>(*id)),
        applyfn(|_, rec, id, _| rec.get::<Creature>(id).unwrap().toughness),
    ));

    rb.describe.add_rule(GenericRule::new(
        whenfn(|_, rec, id| rec.is::<Named>(*id) && rec.is::<Creature>(*id)),
        applyfn(|rb, rec, id, _| {
            let name = &rec.get::<Named>(id).unwrap().name;
            let power = rb.get_power.invoke(rb, rec, id);
            let toughness = rb.get_toughness.invoke(rb, rec, id);
            format!("{} ({}/{} creature)", name, power, toughness)
        }),
    ));
}
